
package bank;

@FunctionalInterface
public interface Penalty {
    public long compute(long minBal, long bal);
}

