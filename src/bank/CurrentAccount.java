package bank;

public class CurrentAccount extends Account {

    private static final long minimumBalance = 10_000;
//    private static final long penalty = 100;
    private static Penalty penalty = (minBal, bal) -> minBal > bal ? 100 : 0;

    public static void setPenalty(Penalty p) {
        penalty = p;
    }
    public CurrentAccount(long acno, String n, long openBal) throws NegativeAmountException {
        super(acno, n, openBal);
    }

    public CurrentAccount(String n, long openBal) throws NegativeAmountException {
        super(n, openBal);
    }

    public boolean withdraw(long amt) throws NegativeAmountException {
        if (!super.withdraw(amt)) {
            return false;
        }
//        if (this.balance < minimumBalance) {
        if (this.getBalance() < minimumBalance) {
//            this.balance -= penalty;
            new Transaction("Penalty", TransType.DEBIT, penalty.compute(minimumBalance, this.getBalance()));
        }
        return true;
    }
}

