class Outer {
    private int x;
    public void m1(Inner1 in1, Inner2 in2) {
        System.out.println(in1.y);
        System.out.println(in2.z);
    }
    class Inner1 {
        private int y;
        public void m2(Outer out, Inner2 in2) {
            System.out.println(out.x);
            System.out.println(in2.z);
        }
    }
    class Inner2 {
        private int z;
        public void m3(Outer out, Inner1 in1) {
            System.out.println(out.x);
            System.out.println(in1.y);
        }
    }
}

